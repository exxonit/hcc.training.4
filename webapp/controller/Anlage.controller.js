sap.ui.define([
	"hcc/training/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("hcc.training.controller.Anlage", {

		onSave: function() {
			var oView = this.getView();
			var oModel = oView.getModel();
			var sPath = "/ZUI5_TEST_00Set";
			var oData = {
				UniqueId: oView.byId("idInputUniqueId").getValue(),
				Ui5Txt01: oView.byId("idInputUi5Txt01").getValue(),
				Ui5Txt02: oView.byId("idInputUi5Txt02").getValue()
			};
			//Entscheidung, ob es sich um CREATE oder UPDATE handelt
			if (oView.byId("idInputUniqueId").getValue() === "") {
				//CREATE
				//Prüfen, ob Email i.O. (mit regulärem Ausdruck)
				var regExMail =
					/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				if (regExMail.test(oView.byId("idInputUi5Txt01").getValue()) === true) {
					oModel.create(sPath, oData);
				} else {
					//keine valide Email-Adresse
					sap.m.MessageToast.show(oView.byId("idInputUi5Txt01").getValue() + " ist keine gültige Email-Adresse");
				}
			} else {
				//UPDATE
				oModel.update(sPath + "('" + oView.byId("idInputUniqueId").getValue() + "')", oData);
			}
		},

		onChange: function(oEvent) {
			if (/\d/.test(oEvent.getSource().getValue()) === true) {
				oEvent.getSource().setValueState(sap.ui.core.ValueState.Error); // if the field contains a digit, it will go red
			} else {
				oEvent.getSource().setValueState(sap.ui.core.ValueState.None); // else, the value state (if any) is removed
			}
		},

		onChangeSelect: function(oEvent) {
			var oView = this.getView();
			oView.byId("SimpleFormDisplay354").bindElement("/ZUI5_TEST_00Set('" + oEvent.getSource().getSelectedKey() + "')");
		}

	});
});
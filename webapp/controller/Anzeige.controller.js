sap.ui.define([
	"hcc/training/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("hcc.training.controller.Anzeige", {

		onAfterRendering: function() {
			//UniqueId wurde bei initialem Laden nicht angezeigt
			//daher manuelles Lesen vom EntitySet, zur Sicherheit 
			this.getView().getModel().read("/ZUI5_TEST_00Set");
		},

		onExit: function() {
			if (this._oPopover) {
				this._oPopover.destroy();
			}
		},

		onActionEdit: function(oEvent) {
			var sPath = "/ZUI5_TEST_00Set";
			if (!this._oPopover) {
				var oCtrl = sap.ui.controller("hcc.training.controller.Anzeige");
				this._oPopover = sap.ui.xmlfragment("hcc.training.view.Delete", oCtrl);
				this.getView().addDependent(this._oPopover);
			}
			var sUniqueId = oEvent.getSource().getCells()["0"].getText();
			this._oPopover.bindElement(sPath + "('" + sUniqueId + "')");
			this._oPopover.openBy(oEvent.getSource());
		},

		handleCloseButton: function(oEvent) {
			//press.Button.Footer.Popover
			oEvent.getSource().getParent().getParent().close();
		},

		handleDeleteButton: function(oEvent) {
			//press.Button.Footer.Popover.Content.Input
			var sUniqueId = oEvent.getSource().getParent().getParent().getContent()["1"].getValue();
			//press.Button.Model
			var oModel = oEvent.getSource().getModel();
			var sPath = "/ZUI5_TEST_00Set";
			oModel.remove(sPath + "('" + sUniqueId + "')");
			oEvent.getSource().getParent().getParent().close();
		}

	});
});
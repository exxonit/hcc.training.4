sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function(Controller, History) {
	"use strict";

	return Controller.extend("exxsens.nexus.controller.BaseController", {
		onInit: function() {
			
		},
		
		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},
		
		onNavBack: function() {
			var oHistory, sPreviousHash;
			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("Main", {}, true /*no history*/ );
			}
		},
		
		onNavToMain: function() {
			this.getRouter().navTo("Main");
		},
		
		onNavToAnzeige: function() {
			this.getRouter().navTo("Anzeige");
		},
		
		onNavToAnlage: function() {
			this.getRouter().navTo("Anlage");
		}		
		
	});
});